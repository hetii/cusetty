# CuseTTY #

  CuseTTY is software bridge between virtual TTY character device and any TCP endpoint,
  for eg it can be used with esp-link to pass trafic to it as well as port settings.
 
  Author: Grzegorz Hetman
  email: ghetman@gmail.com
 
  Few words about license.
  You can do whatever you want to do with this code for non comercial usage,
  otherwise please consider some donation on my paypal email^, or contact me directly for more details.
 
### How do I get set up? ###

* Install libfuse-dev and gcc/make
* Run make to compile it.
* Execute cusetty with destination IP.
* Connect your terminal software using /dev/ttyCUSE0 character device.
*
* Optionally run echoserver.py to see trafic from cusetty.