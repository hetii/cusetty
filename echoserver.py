# Copyright (c) Twisted Matrix Laboratories.
# See LICENSE for details.

from twisted.internet import reactor, protocol
from twisted.internet import task
from twisted.internet import reactor


class Echo(protocol.Protocol):
    """This is just about the simplest possible protocol"""
    
    def dataReceived(self, data):
        "As soon as any data is received, write it back."
	print "GET DATA:", data
        self.transport.write(data)

    def connectionMade(self):

        def called(result):
            print result

	def f(s):
            self.transport.write(s)
	    return "This will run 3.5 seconds after it was scheduled: %s" % s

        #d = task.deferLater(reactor, 3.5, f, "hello, world")
        #d.addCallback(called)

	print "Connection made!:)"

def main():
    """This runs the protocol on port 8000"""
    factory = protocol.ServerFactory()
    factory.protocol = Echo
    reactor.listenTCP(23, factory)
    reactor.listenTCP(80, factory)
    reactor.run()

# this only runs if the module was *not* imported
if __name__ == '__main__':
    main()
