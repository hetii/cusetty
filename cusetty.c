/* 
 * CuseTTY is software bridge between virtual TTY character device and any TCP endpoint,
 * for eg it can be used with esp-link to pass trafic to it as well as port settings.
 *
 * Author: Grzegorz Hetman
 * email: ghetman@gmail.com
 *
 * Few words about license.
 * You can do whatever you want to do with this code for non comercial usage,
 * otherwise please consider some donation on my paypal email^, or contact me directly for more details.
 *
 * Usage: 
 */
  
#define FUSE_USE_VERSION 30
#define _FILE_OFFSET_BITS 64

//#include <asm-generic/termios.h>
#include <termios.h>
#include <sys/ioctl.h>
#define TIOCM_LOOP	0x8000


#include <fuse/cuse_lowlevel.h>
#include <fuse/fuse_opt.h>
#include <linux/kd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>  
#include <stdlib.h>
#include <strings.h>
#include <errno.h>
#include <poll.h>
#include <netdb.h> 

struct Mytermio {
	unsigned short c_iflag;		/* input mode flags */
	unsigned short c_oflag;		/* output mode flags */
	unsigned short c_cflag;		/* control mode flags */
	unsigned short c_lflag;		/* local mode flags */
	unsigned char c_line;		/* line discipline */
	unsigned char c_cc[NCC];	/* control characters */
};

char *hostname;

typedef struct fifo_t {
    char *buffer;
    unsigned int head;
    unsigned int tail;
    size_t size;
} fifo_t;

typedef char byte_t;

fifo_t* fifo_init(size_t size){
    byte_t* buffer = (byte_t*)malloc(size);

    if (buffer == NULL)
        return NULL;

    fifo_t* fifo = (fifo_t*)malloc(sizeof(fifo_t));

    if (fifo == NULL) {
       free(buffer);
       return NULL;
    }

    fifo->buffer = buffer;
    fifo->head = 0;
    fifo->tail = 0;
    fifo->size = size;
    return fifo;
}

uint8_t fifo_is_full(fifo_t* fifo){
    if ((fifo->head == (fifo->size - 1) && fifo->tail == 0) || (fifo->head == (fifo->tail - 1)))
        return 1;
    else
        return 0;
}

size_t fifo_push_byte(fifo_t* fifo, byte_t byte){

    if (fifo == NULL) return 0;

    if (fifo_is_full(fifo) == 1)
       return 0;

    fifo->buffer[fifo->head] = byte;

    fifo->head++;
    if (fifo->head == fifo->size)
       fifo->head = 0;

    return 1;
}

size_t fifo_push_bytes(fifo_t* fifo, byte_t* bytes, size_t count){

    if (fifo == NULL) return 0;

    for (uint32_t i = 0; i < count; i++){
        if (fifo_push_byte(fifo, bytes[i]) == 0)
            return i;
    }

    return count;
}

uint8_t fifo_is_empty(fifo_t* fifo){
    if (fifo->head == fifo->tail)
        return 1;
    else
        return 0;
}

size_t fifo_pop_byte(fifo_t* fifo, byte_t* byte){

    if (fifo == NULL) return 0;

    if (fifo_is_empty(fifo) == 1)
        return 0;

    *byte = fifo->buffer[fifo->tail];

    fifo->tail++;
    if (fifo->tail == fifo->size)
        fifo->tail = 0;

    return 1;
}

size_t fifo_pop_bytes(fifo_t* fifo, byte_t* bytes, size_t count){
    
    if (fifo == NULL) return 0;

    for (uint32_t i = 0; i < count; i++){
        if (fifo_pop_byte(fifo, bytes + i) == 0)
            return i;
    }

    return count;
}

size_t fifo_bytes_filled(fifo_t* fifo){
    if (fifo->head == fifo->tail)
        return 0;
    else if ((fifo->head == (fifo->size - 1) && fifo->tail == 0) || (fifo->head == (fifo->tail - 1)))
        return fifo->size;
    else if (fifo->head < fifo->tail)
        return (fifo->head) + (fifo->size - fifo->tail);
    else
        return fifo->head - fifo->tail; 
}

void fifo_flush(fifo_t* fifo){
    if (fifo != NULL) {
        fifo->head = 0;
        fifo->tail = 0;
    }
}

fifo_t *input_fifo;  // This is a fifo for data that comes from ttyCUSE.
fifo_t *output_fifo; // This is a fifo for data from TCP endpoint.

// ==================== END OF FIFO STUFF ==================== //

pthread_t read_thread_id;
pthread_t write_thread_id;

int socket_telnet = -1;

int init_connection(char *host, unsigned int port){
    
    struct sockaddr_in serveraddr;
    struct hostent *server;

    int sockfd = socket(AF_INET, SOCK_STREAM, 0);

    if (sockfd < 0) {
        perror("ERROR: opening socket\n");
        return sockfd;
    }

    // Resolve the server's DNS entry.
    server = gethostbyname(host);
    if (server == NULL) {
        perror("ERROR: not valid host");
        return -1;
    }

    // Build the server's Internet address.
    bzero((char *) &serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, (char *)&serveraddr.sin_addr.s_addr, server->h_length);
    serveraddr.sin_port = htons(port);

    // connect: create a connection with the server
    if (connect(sockfd, (const struct sockaddr *) &serveraddr, sizeof(serveraddr)) < 0) {
        perror("ERROR: connecting\n");
        return -1;
    }
    return sockfd;
}

static void *read_from_socket(void *arg){
    size_t size = 0;
    char bufr[4096];
    fd_set readfds;
    fd_set writefds;


    if(pthread_equal(pthread_self(), read_thread_id)){
        printf("Reding thread processing\n");
    }
    // call this when you are not ready to cancel the thread
    // pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);

    // Call this when we are ready to cancel the thread.
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);


    while (1){
        usleep(10);
        //clear the socket set
        FD_ZERO(&readfds);
        FD_ZERO(&writefds);
  
        //add master socket to set
        FD_SET(socket_telnet, &readfds);
        FD_SET(socket_telnet, &writefds);
        // Block this loop till some activity happen.

        int activity = select(socket_telnet +1, &readfds , &writefds , NULL , NULL);        
        if ((activity < 0) && (errno!=EINTR)){
            printf("select error\n");
        }

        // Socket is ready for writting.
        if (FD_ISSET(socket_telnet, &writefds)){
            size_t input_fifo_length = fifo_bytes_filled(input_fifo);

            // Ok socket is ready but do we have some data for it?
            if (input_fifo_length > 0) {

                // printf("Ready for writting.\n");
                char* buffer = (char*)malloc(input_fifo_length);
                fifo_pop_bytes(input_fifo, buffer, input_fifo_length);                
                size_t ret = write(socket_telnet, buffer, input_fifo_length);

                free(buffer);
                if (ret < 0) {
                    perror("ERROR: writing to socket");
                } else if (ret != input_fifo_length){
                    perror("Different amount of data was writing.");
                }
            }
        }
        
        // Socket is ready for reading.
        if (FD_ISSET(socket_telnet, &readfds)) {
            // printf("Ready for ridding\n");
            size = read(socket_telnet, bufr, 4096);

            if (size < 0) {
                perror("ERROR reading from socket\n");
                pthread_exit(NULL);
            }
            fifo_push_bytes(output_fifo, bufr, size);
        }
    }
    return NULL;
}

static void tcp_txrx(char *fmt, char *rate){
    int sockfd = init_connection(hostname, 80);
    int total, sent, bytes;
    // int received;    
    // char response[4096];
    char message[1024]; 
    
    if (fmt != NULL) {   
        char *message_fmt = "POST /console/fmt?fmt=%s HTTP/1.0\r\n\r\n";
        sprintf(message, message_fmt, fmt);
    } else if (rate != NULL){
        char *message_fmt = "POST /console/baud?rate=%s HTTP/1.0\r\n\r\n";
        sprintf(message, message_fmt, rate);
    }

    printf("Make http request: |%s|\n", message);

    /* send the request */
    total = strlen(message);
    sent = 0;
    do {
        bytes = write(sockfd, message + sent, total - sent);
        if (bytes < 0)
            perror("ERROR writing message to socket\n");
        if (bytes == 0)
            break;
        sent += bytes;
    } while (sent < total);
/*
    // receive the response
    memset(response,0,sizeof(response));
    total = sizeof(response)-1;
    received = 0;
    do {
        bytes = read(sockfd,response+received,total-received);
        if (bytes < 0)
            perror("ERROR reading response from socket\n");
        if (bytes == 0)
            break;
        received += bytes;
    } while (received < total);
    if (received == total)
        perror("ERROR storing complete response from socket\n");

    // process response
    printf("Response:\n%s\n", response); 
*/
    close(sockfd);
}

// ==================== END OF NETWORKING ==================== //

static void serialtcp_poll(fuse_req_t req, struct fuse_file_info *fi, struct fuse_pollhandle *ph){
    // fuse_reply_poll(req, POLLIN);  // rlist -- wait until ready for reading
    // fuse_reply_poll(req, POLLOUT); // wlist -- wait until ready for writing
    unsigned int mask = 0;
    
    // Allow reading just when we have some data.
    // Of course client can use timeout and don`t wait 
    // for data.
    size_t output_fifo_length = fifo_bytes_filled(output_fifo);
    if (output_fifo_length  > 0){
        mask |= POLLIN;
    }

    if (!fifo_is_full(input_fifo)){
        mask |= POLLOUT;
    }

    fuse_reply_poll(req, mask);
    //fuse_reply_poll(req, POLLIN|POLLOUT);
}

// Whatever I read from ttyCUSE0 is append to input_fifo.
static void serialtcp_write(fuse_req_t req, const char *buf, size_t size, off_t off, struct fuse_file_info *fi){
    // Write data to internal fifo buffer.    
    fifo_push_bytes(input_fifo, (char *)buf, size);
    fuse_reply_write(req, size);    
}

// Whatever we have in output_fifo, we send it to ttyCUSE0 (all or just chunk)
static void serialtcp_read(fuse_req_t req, size_t size, off_t off, struct fuse_file_info *fi){
    
    // Send amount of data that we was asked for or if not asked
    // then send all.
    size_t output_fifo_length = fifo_bytes_filled(output_fifo);

    size_t s = size < output_fifo_length ? size : output_fifo_length ;

    char* buffer = (char*)malloc(s);
    
    fifo_pop_bytes(output_fifo, buffer, s);
    fuse_reply_buf(req, buffer, s);

    free(buffer);
}

static void serialtcp_open(fuse_req_t req, struct fuse_file_info *fi){  
    if (socket_telnet == -1){
        //socket_telnet = init_connection("127.0.0.1", 8000);
        socket_telnet = init_connection(hostname, 2323);

        int err = pthread_create(&read_thread_id, NULL, &read_from_socket, NULL);
        if (err != 0)
            printf("\ncan't create thread :[%s]", strerror(err));

    }

    fuse_reply_open(req, fi);
}

static void serialtcp_ioctl(fuse_req_t req, int cmd, void *arg, struct fuse_file_info *fi, unsigned flags, const void *in_buf, size_t in_bufsz, size_t out_bufsz) {
    
    unsigned int tiocm = 0;
    static char fmt[1024];
    static unsigned int mcr = 0;
    static unsigned int old_mcr = 0;
    static struct termios tio = {
        .c_cflag = B115200|CS8|PARODD|CREAD|CLOCAL,
    };

    if ((cmd == TIOCMBIS) ||
        (cmd == TIOCMBIC) ||
        (cmd == TIOCMSET)) {

      if (!in_bufsz){
          struct iovec iov = { arg, sizeof(unsigned int) };
          fuse_reply_ioctl_retry(req, &iov, 1, NULL, 0);
      } else {
          // Get single bit for TIOCMXXX call.
          tiocm = *(unsigned int *)in_buf;
          fuse_reply_ioctl(req, 0, 0, 0);
      }
    }

    switch(cmd){
        case TCSETSF:
        case TCSETSW:
        case TCSETS:
            if (!in_bufsz){
                struct iovec iov = { arg, sizeof(struct termios) };
                fuse_reply_ioctl_retry(req, &iov, 1, NULL, 0);
            } else {
                memcpy(&tio, in_buf, in_bufsz);
                sprintf(fmt, "%c%c%c", 
                (tio.c_cflag & CSIZE) == CS5 ? '5': 
                (tio.c_cflag & CSIZE) == CS6 ? '6': 
                (tio.c_cflag & CSIZE) == CS7 ? '7': '8',
                (tio.c_cflag & PARENB && tio.c_iflag & IGNPAR)? 'X':
                (tio.c_cflag & PARENB && tio.c_cflag & CMSPAR && tio.c_cflag & PARODD)? 'M':
                (tio.c_cflag & PARENB && tio.c_cflag & CMSPAR)? 'S':
//                (tio.c_cflag & PARENB && tio.c_cflag & PARODD)? 'O':
                (tio.c_cflag & PARENB)? 'E': 'N', 
                 tio.c_cflag & CSTOPB ? '2': '1');

                char speed_str[10];

                speed_t speed;
                speed = cfgetospeed(&tio);
                printf( "Output baud: %ld\n", (long int)speed );

                switch(speed){
                    case 0:
                      strncpy(speed_str, "0", 9);
                      break;
                    case 1:
                      strncpy(speed_str, "50", 9);
                      break;
                    case 2:
                      strncpy(speed_str, "75", 9);
                      break;
                    case 3:
                      strncpy(speed_str, "110", 9);
                      break;
                    case 4:
                      strncpy(speed_str, "134", 9);
                      break;
                    case 5:
                      strncpy(speed_str, "150", 9);
                      break;
                    case 6:
                      strncpy(speed_str, "200", 9);
                      break;
                    case 7:
                      strncpy(speed_str, "300", 9);
                      break;
                    case 8:
                      strncpy(speed_str, "600", 9);
                      break;
                    case 9:
                      strncpy(speed_str, "1200", 9);
                      break;
                    case 10:
                      strncpy(speed_str, "1800", 9);
                      break;
                    case 11:
                      strncpy(speed_str, "2400", 9);
                      break;
                    case 12:
                      strncpy(speed_str, "4800", 9);
                      break;
                    case 13:
                      strncpy(speed_str, "9600", 9);
                      break;
                    case 14:
                      strncpy(speed_str, "19200", 9);
                      break;
                    case 15:
                      strncpy(speed_str, "38400", 9);
                      break;
                    case 4097:
                      strncpy(speed_str, "57600", 9);
                      break;
                    case 4098:
                      strncpy(speed_str, "115200", 9);
                      break;
                    case 4099:
                      strncpy(speed_str, "230400", 9);
                      break;
                }
                printf("Speed is:%s\n", speed_str);
                tcp_txrx(NULL, speed_str);

                tcp_txrx(fmt, NULL);

                // https://viereck.ch/linux-mark-space-parity/
                // printf("PARODD: %d\n", tio.c_cflag & PARODD ? 1:0); //   O M
                // printf("CMSPAR: %d\n", tio.c_cflag & CMSPAR ? 1:0); //     M S
                // printf("PARENB: %d\n", tio.c_cflag & PARENB ? 1:0); // E O M S
                // printf("CSTOPB: %d\n", tio.c_cflag & CSTOPB ? 1:0); //

                fuse_reply_ioctl(req, 0, 0, 0);
            }
        break;
        case TCGETS:
            if(out_bufsz){
                fuse_reply_ioctl(req, 0, &tio, sizeof(struct termio));
            } else {
                struct iovec iov = { arg, sizeof(struct termio) };
                fuse_reply_ioctl_retry(req , NULL, 0, &iov, 1);
            }
        break;

        case TIOCINQ: // Get the number of bytes in the input buffer (serialtcp_write).
            printf("TIOCINQ called\n");
            if(out_bufsz){
                size_t fifo_rx_size = fifo_bytes_filled(input_fifo);
                fuse_reply_ioctl(req, 0, &fifo_rx_size, sizeof(size_t));
            } else {
                struct iovec iov = { arg, sizeof(size_t) };
                fuse_reply_ioctl_retry(req , NULL, 0, &iov, 1);
            }

        break;
        case TIOCOUTQ: // Get the number of bytes in the output buffer (serialtcp_read).
            printf("TIOCOUTQ called\n");
            if(out_bufsz){
                size_t output_fifo_length  = fifo_bytes_filled(output_fifo);
                fuse_reply_ioctl(req, 0, &output_fifo_length , sizeof(size_t));
            } else {
                struct iovec iov = { arg, sizeof(size_t) };
                fuse_reply_ioctl_retry(req , NULL, 0, &iov, 1);
            }
        break;
        // See below code:
        case TCFLSH:
            if (arg == TCIFLUSH){
                printf("Flush: output_fifo\n");
                fifo_flush(output_fifo);
            } else if ((size_t)arg == TCOFLUSH){
                printf("Flush: input_fifo\n");
                fifo_flush(input_fifo);
            } else if ((size_t)arg == TCIOFLUSH){
                printf("Flush: input_fifo and output_fifo\n");
                fifo_flush(input_fifo);
                fifo_flush(output_fifo);
            }
            fuse_reply_ioctl(req, 0, 0, 0);
        break;
        // Not sure if those 3 below ioctl can be called directly or just used in TCFLSH.
        // flushes the input queue, which contains data that have been received but not yet read. 
        case TCIFLUSH: 
            fifo_flush(output_fifo);
            fuse_reply_ioctl(req, 0, 0, 0);
        break;
        // flushes the output queue, which contains data that have been written but not yet transmitted. 
        case TCOFLUSH: 
            fifo_flush(input_fifo);
            fuse_reply_ioctl(req, 0, 0, 0);
        break;
        // flushes both the input and output queue.
        case TCIOFLUSH: 
            fifo_flush(input_fifo);
            fifo_flush(output_fifo);
            fuse_reply_ioctl(req, 0, 0, 0);
        break;

        case TIOCMGET:
            if(out_bufsz){
                fuse_reply_ioctl(req, 0, &mcr, sizeof(unsigned int));
            } else {
                struct iovec iov = { arg, sizeof(unsigned int) };
                fuse_reply_ioctl_retry(req , NULL, 0, &iov, 1);
            }
        break;
        case TIOCMBIS:
            mcr |= (tiocm & TIOCM_RTS)
                |  (tiocm & TIOCM_DTR)
                |  (tiocm & TIOCM_LOOP);
        break;
        case TIOCMBIC:
            mcr &= ~((tiocm & TIOCM_RTS)
                |    (tiocm & TIOCM_DTR)
                |    (tiocm & TIOCM_LOOP));
        break;
        case TIOCMSET:
            // Turn off the RTS and DTR and LOOPBACK, 
            // and then only turn on what was asked for.
            mcr &=  ~(TIOCM_RTS | TIOCM_DTR | TIOCM_LOOP);
            mcr |= (tiocm & TIOCM_RTS)
                |  (tiocm & TIOCM_DTR)
                |  (tiocm & TIOCM_LOOP);
        break;
        case TCSBRK: // termios.tcdrain ?
            fuse_reply_ioctl(req, 0, 0, 0);
        break;
        default:
            //printf("unhandled cmd=0x%x (%s) . arg=%p as ENOSYS\n",cmd, cmdname(cmd) , arg);
            fuse_reply_err(req, ENOSYS);
        return;
    }

    // Check if something change on DTR/RTS/LOOP and do some acction.
    if (mcr !=  old_mcr){
        old_mcr = mcr;
        printf("RTS is:  %d\n", mcr & TIOCM_RTS  ? 1:0 );
        printf("DTR is:  %d\n", mcr & TIOCM_DTR  ? 1:0 );
        printf("LOOP is: %d\n", mcr & TIOCM_LOOP ? 1:0 );
    }
}
    
static void serialtcp_release(fuse_req_t req, struct fuse_file_info *fi){
    
    fuse_reply_err(req, 0);
}

static const struct cuse_lowlevel_ops serialtcp_clop = {
    .open    = serialtcp_open,
    .read    = serialtcp_read,
    .write   = serialtcp_write,
    .ioctl   = serialtcp_ioctl,
    .release = serialtcp_release,
    .poll    = serialtcp_poll
};


int main(int argc, char **argv) {

    // Define two fifos, one for data that we get from character device
    // and second for data that comes form TCP endpoint.
    input_fifo = fifo_init(4096);
    output_fifo = fifo_init(4096);

    //const char* cusearg[] = {"test", "-f"};
    const char* cusearg[] = {"test", "-f", "-d"};
    const char* devarg[]  = {"DEVNAME=ttyCUSE0" };
    hostname = argv[1];
   
    printf("Hostname: %s\n", hostname);
    struct cuse_info ci;
    memset(&ci, 0x00, sizeof(ci));
    ci.flags = CUSE_UNRESTRICTED_IOCTL;
    ci.dev_info_argc=1;
    ci.dev_info_argv = devarg;
    
    return cuse_lowlevel_main(2, (char**) &cusearg, &ci, &serialtcp_clop, NULL);
}
