all: clean
	@make cuse
	@sudo ./cusetty 192.168.0.17 2323

clean:
	@rm -rf *.o cusetty

cuse:
	@gcc -Wall -g cusetty.c -lfuse  -lpthread -o cusetty
